package main

import (
	"fmt"
)

/*
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
The sum of these multiples is 23.
Find the sum of all the multiples of 3 or 5 below 1000.
*/

func main() {
	x := 1000
	i := 1
	dividers := [2]int{3, 5}
	var total int
	for i < x  {
		for _, divider := range dividers {
			if i % divider == 0 {
				total += i
				fmt.Printf(string("%d is multiple of %d\n"), i, divider)
			}
		}
		i += 1
	}
	fmt.Printf(string("total is: %d"), total)
}
