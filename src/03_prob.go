package main

import (
	"fmt"
)

/*
The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?
 */

const NUMBER int = 600851475143

func is_prime(n int) bool {
	for i := 2; i <= int(n / 2); i++ {
		if n % i == 0 {
			fmt.Println(n, "is not a prime number")
			fmt.Println(i, "times", n/i, "is", n)
			return false
		}
	}
	return true
}

func main() {
	var prime_factors []int
	for i := 2; i <= int(NUMBER / 2); i++ {
		if NUMBER % i == 0 {
			if is_prime(i) {
				prime_factors = append(prime_factors, i)
			}
		}
	}
	fmt.Printf(string("prime factors for %d are: %v \n"), NUMBER, prime_factors)
}