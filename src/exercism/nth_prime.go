package main

import (
	"os"
	"fmt"
	"strconv"
)

/*
# Nth Prime
Given a number n, determine what the nth prime is.

## Running the tests
To run the tests run the command `go test` from within the exercise directory.

## Source
A variation on Problem 7 at Project Euler [http://projecteuler.net/problem=7](http://projecteuler.net/problem=7)
*/

func is_prime(n int) bool {
	if n == 2 {
		return true
	} else if n == 3 {
		return true
	} else if n % 2 == 0 {
		return false
	} else if n % 3 == 0 {
		return false
	} else {
		i := 5
		w := 2
		for i * i <= n {
			if n % i == 0 {
				return false
			}
			i += w
			w = 6 - w
		}
	}
	return true
}

func main() {
	var input string = os.Args[1]
	n, _ := strconv.Atoi(input)

	nth := 0
	nth_prime := 0

	for i := 2; nth < n; i++{
		if is_prime(i) {
			nth += 1
			nth_prime = i
		}
	}
	fmt.Print(n, "th prime number is: ", nth_prime)
}