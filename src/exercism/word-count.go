package main

import (
	"os"

	"strings"

	"fmt"
)

/*
Given a phrase, count the occurrences of each word in that phrase.
*/


func main(){
	var input string = os.Args[1]
	var words []string
	dict := make(map[string]int)

	words = strings.Fields(input)


	for _, word := range words {
		value, ok := dict[word]
		if ok{
			dict[word] = value + 1
		} else {
			dict[word] = 1
		}
	}

	for k, v := range dict{
		fmt.Println("Count:", v, "Word:", k)
	}
}
